<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\ToDoListService;

class ToDoListServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Services\ToDoListService', function ($app) {
            return new ToDoListService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
