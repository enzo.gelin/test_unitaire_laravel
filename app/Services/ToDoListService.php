<?php

namespace App\Services;

use App\Models\Item;
use App\Models\Todolist;
use Carbon\Carbon;
use Exception;

class ToDoListService
{
    // Todolist creation
    public function verifFirst($id)
    {
        return $this->checkTotalTodolist($id);
    }
    // Item creation and email sending constraint
    public function verifSecond(Item $item, Todolist $todolist)
    {
        $lenghtList = $this->isLengthValid($todolist);
        $verifItem = $item->isValid();
        $lastInsertItem = $this->checkTodolistLastItemTimer($todolist);

        if ($lenghtList == true && $verifItem == true && $lastInsertItem == true) {
            $tests = 7;
            if ($tests == 7) {
                // si longueur est egal a 7 envoyé un mail
                $emailService = new EmailService();
                $emailState = $emailService->sendMail($todolist);
            }
            return true;
        } else {
            return false;
        }
    }

    public function isValid(Todolist $todolist){
        if(!empty($todolist->name) && strlen($todolist->name)<= 256){
            return true;
        }else{
            return false;
        }
    }

    public function isLengthValid(Todolist $todolist)
    {
        if($this->actualItemsCount($todolist) >= 10){
             return false;
        }else{
            return true;
        }
    }

    public function actualItemsCount(Todolist $todolist){
        return count(Item::where('todolist_id', $todolist->id)->get());
    }

    public function getLastItem(Todolist $todolist){
         return Item::where('todolist_id', $todolist->id)->latest('id')->first();
    }

    public function checkTodolistLastItemTimer(Todolist $todolist){
        $lastItem = $this->getLastItem($todolist);
        if(!is_null($this->getLastItem($todolist)) && Carbon::now()->subMinutes(30)->isBefore($lastItem->datecreated)){
            return false;
        }
        return true;
    }

    public function canAddItem(Item $item, Todolist $todolist){
        if(is_null($item)){
            throw new Exception ('Item est vide');
            return false;
        }
        if(is_null($todolist->user) || !$todolist->user->isValid()){
            throw new Exception ('To do list non valide');
            return false;
        }
        if($this->actualItemsCount($todolist) >= 10){
            throw new Exception ('To do list longueur erreur');
            return false;
        }
        $lastItem = $this->getLastItem($todolist);
        if(!is_null($this->getLastItem($todolist)) && Carbon::now()->subMinutes(30)->isBefore($lastItem->datecreated)){
            throw new Exception ('Dernier item créé il y a peu de temps');
            return false;
        }
        return true;

    }


    // Obsolete

    // Verifie que l'user a une seul todolist et si elle est deja créé
    public function checkTotalTodolist($id)
    {
        $todolist = Todolist::where('user_id', $id)->get();
        if (count($todolist) < 1) {
            $todolist = true;
        } else {
            $todolist = false;
        }
        //var_dump($todolist);
        return $todolist;
    }


    // Verifie le nomet le contenu de l'item
    public function verifItem(Item $item)
    {
        // check lenght name
        $items = Item::where('name', $item->name)->get();
        if (count($items) > 0 || strlen($item->content) >= 1000 ) {
            return false;
        } else {
            return true;
        }
    }

    // Verifie si le dernier item a ete créé il y a + de 30 min
    public function lastInsertItem($id)
    {
        $todolist = Todolist::where('user_id', $id)->first();
        $items = Item::where('todolist_id', $todolist->id)->orderBy('created_at', 'desc')->first();

        $datelastitem = new Carbon($items->created_at);
        $datelastitem = $datelastitem->addMinutes(30);
        $date = new Carbon('UTC');

        if (strtotime($datelastitem) >= strtotime($date)) {
            // echo 'crea imppossible';
            $result = false;
        } else {
            // echo 'crea possible';
            $result = true;
        }
        return $result;
    }
}
