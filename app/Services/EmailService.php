<?php

namespace App\Services;

use App\Mail\MailNotify;
use App\Models\Todolist;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Mail;

class EmailService
{
    public function sendMail(Todolist $todolist){       
        $datebirth = $todolist->user->birthday;
        $date = new Carbon('UTC');
        if ($todolist->user->checkBirthday($todolist->user->birthday)) {
            // ENvoi de mail erreur
            Mail::fake();
            Mail::to('test@email.fr')->send(new MailNotify());
            Mail::assertSent(MailNotify::class);
            return true;
        } else {
            throw new Exception ('Mail non envoyé');
            return false;
        }
    }
}
