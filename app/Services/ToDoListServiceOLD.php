<?php

namespace App\Services;

use App\Models\Item;
use App\Models\Todolist;
use Carbon\Carbon;
use Exception;

class ToDoListService
{
    // Todolist creation
    public function verifFirst($id)
    {
        return $this->checkTotalTodolist($id);
    }
    // Item creation and email sending constraint
    public function verifSecond($id, $item_content, $item_name)
    {
        $lenghtList = $this->checkLenghtTodolist($id);
        $verifItem = $this->verifItem($item_content, $item_name);
        $lastInsertItem = $this->lastInsertItem($id);
        if ($lenghtList == true && $verifItem == true && $lastInsertItem == true) {
            $todolist = Todolist::where('user_id', $id)->first();
            $items = Item::where('todolist_id', $todolist->id)->get();
            if (count($items) == 7) {
                // si longueur est egal a 7 envoyé un mail
                $emailService = new EmailService();
                $customServiceInstance = $emailService->sendMail($id);
            }
            return true;
        } else {
            return false;
        }
    }

    public function isLengthValid(Todolist $todolist)
    {
        if($this->actualItemsCount($todolist) >= 10){
             return false;
        }else{
            return true;
        }
    }

    public function actualItemsCount(Todolist $todolist){
        return count(Item::where('todolist_id', $todolist->id)->get());
    }

    public function getLastItem(Todolist $todolist){
        return Item::where('todolist_id', $todolist->id)->latest('id')->first()->get();
    }

    public function checkTodolistLastItemTimer(Todolist $todolist){
        $lastItem = $this->getLastItem($todolist);
        if(!is_null($this->getLastItem($todolist)) && Carbon::now()->subMinutes(30)->isBefore($lastItem->created_at)){
            return false;
        }
        return true;
    }

    public function canAddItem(Item $item, Todolist $todolist){
        if(is_null($item)){
            return false;
        }
        if(is_null($todolist->user) || !$todolist->user->isvalid()){
            return false;
        }
        if($this->actualItemsCount($todolist) >= 10){
            return false;
        }
        $lastItem = $this->getLastItem($todolist);
        if(!is_null($this->getLastItem($todolist)) && Carbon::now()->subMinutes(30)->isBefore($lastItem->created_at)){
            return false;
        }
        return true;

    }

    // Verifie que l'user a une seul todolist et si elle est deja créé
    public function checkTotalTodolist($id)
    {
        $todolist = Todolist::where('user_id', $id)->get();
        if (count($todolist) < 1) {
            $todolist = true;
        } else {
            $todolist = false;
        }
        //var_dump($todolist);
        return $todolist;
    }

    // Verifie la nombre d'item present dans la liste
    public function checkLenghtTodolist($id)
    {
        $todolist = Todolist::where('user_id', $id)->first();
        $items = Item::where('todolist_id', $todolist->id)->get();
        // si longueur supérieur a 10 creation impossible
        if (count($items) > 9) {
            $result = false;
        } else {
            $result = true;
        }
        return $result;
    }

    // Verifie le nomet le contenu de l'item
    public function verifItem($item_content, $item_name)
    {
        // check lenght name
        $items = Item::where('name', $item_name)->get();
        if (count($items) > 0) {
            return false;
        } else {
            return true;
        }
        // check lenght content
        if (strlen($item_content) >= 1000) {
            return false;
        } else {
            return true;
        }
    }

    // Verifie si le dernier item a ete créé il y a + de 30 min
    public function lastInsertItem($id)
    {
        $todolist = Todolist::where('user_id', $id)->first();
        $items = Item::where('todolist_id', $todolist->id)->orderBy('created_at', 'desc')->first();

        $datelastitem = new Carbon($items->created_at);
        $datelastitem = $datelastitem->addMinutes(30);
        $date = new Carbon('UTC');

        if (strtotime($datelastitem) >= strtotime($date)) {
            // echo 'crea imppossible';
            $result = false;
        } else {
            // echo 'crea possible';
            $result = true;
        }
        return $result;
    }
}
