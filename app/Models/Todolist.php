<?php

namespace App\Models;

use App\Models\Item;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Exception;

class Todolist extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'user_id',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // A todo list has many items
    public function items(){
        return $this->hasMany(Item::class);
    }

    public function isValid(){
        if(!empty($this->name) && strlen($this->name)<= 256){
            return true;
        }else{
            return false;
        }
    }

    public function actualItemsCount(){
       // return count(Item::where('todolist_id', $this->id)->get());
       sizeof($this->items->get());
    }

    public function getLastItem(){
        //return Item::where('todolist_id', $this->id)->latest('id')->first()->get();
        return $this->items->first();
    }

    public function isLengthValid()
    {
        if($this->actualItemsCount() >= 10){
            return false;
        }else{
            return true;
        }
    }

    public function checkTodolistLastItemTimer(){
        $lastItem = $this->getLastItem();
        if(!is_null($this->getLastItem()) && Carbon::now()->subMinutes(30)->isBefore($lastItem->datecreated)){
            return false;
        }
        return true;
    }


    /*takes an Item then tests

    :
    -> if the item is not null and is valid (length <=1000)
    -> if the user is valid and is not null
    -> if the number of item of the todolist is < 10
    -> if the date of adding of last item is > 30 min
    */
    public function canAddItem(Item $item){
        if(is_null($item) || !$item->isValid()){
            return false;
        }
        if(is_null($this->user) || !$this->user->isvalid()){
            return false;
        }
        if($this->actualItemsCount() >= 10){
            return false;
        }
        $lastItem = $this->getLastItem();
        if(!is_null($this->getLastItem()) && Carbon::now()->subMinutes(30)->isBefore($lastItem->datecreated)){
            return false;
        }
        return true;

    }



}
