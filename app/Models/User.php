<?php

namespace App\Models;

use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'lastname',
        'firstname',
        'birthday',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getTitle()
    {
        return 'Titre';
    }

    public function isValid()
    {

        $name = $this->checkName();
        $birthday = $this->checkBirthday();
        $email = $this->checkMail();
        $pwd = $this->checkPassword();

        if ($name == true && $birthday == true && $pwd == true &&  $email == true) {
            return true;
        } 
        return false;
    }

    // Verifie l'intégrité du nom et prenom
    public function checkName()
    {
        return (strlen($this->firstname) > 2  && strlen($this->lastname) > 2);
    }

    // Verifie l'intégrité de la date et +18 ans
    public function checkBirthday()
    {
        if (carbon::now()->subYears(18)->isAfter($this->birthday)){
            return true;
        }else{
            return false;
        }
    }

    public function checkMail()
    {
        if (filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            $email = true;
        } else {
            $email = false;
        }
        return $email;
    }

    public function checkPassword()
    {
        if (strlen($this->password) >= 8 && strlen($this->password) <= 40){
            return true;
        }else{
            return false;
        }
    }
    //Is given user is meant to have ONE todolist filled with max 10 items
    public function todoList(){
        return $this->hasOne(Todolist::class);
    }
}
