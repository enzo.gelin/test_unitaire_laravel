<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'content',
        'user_id',
        'todolist_id',
        'datecreated'
    ];


     /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function todolist()
    {
        return $this->belongsTo(Todolist::class);
    }

    //an items content shoul not be empty and should be of a length that is <=1000 caracters
    public function isValid(){
        if(!empty($this->content) && strlen($this->content)<= 1000){
            return true;
        }
        return false;
    }
}
