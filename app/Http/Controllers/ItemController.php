<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Item;
use App\Models\Todolist;
use Illuminate\Http\Request;
use App\Services\ToDoListService;
use Illuminate\Support\Facades\Auth;
use Exception;

class ItemController extends Controller
{
    private $customServiceInstance;

    public function __construct(ToDoListService $customServiceInstance)
    {
        $this->customServiceInstance = $customServiceInstance;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $todolist)
    {
        // Check if user have a todolist already
        $item = new Item();
        $item->name = $request->name;
        $item->content = $request->content;
        $item->user_id = $request->user_id;
        $item->todolist_id = $todolist;
        $item->datecreated = $request->datecreated;

        $todoList = Todolist::where('id', $todolist)->first();
        $errors = "";
 
        
        $verif = $this->customServiceInstance->verifSecond($item,$todoList);
        
        if($verif == true){
            $data = array(
                'name'      =>   $item->name,
                'content' => $item->content,
                'user_id'   =>   $item->user_id,
                'todolist_id' => $item->todolist_id,
                'datecreated' => Carbon::now()
            );
            Item::create($data);
            
            return response()->json("Tout est ok : item bien ajoute", 200); 
        } else {
            if($this->customServiceInstance->isLengthValid($todoList) == false){
                $errors .= "10 item atteint. ";
            }
            if($item->isValid() == false){
                $errors .= "Item invalid. ";
            }
            if($this->customServiceInstance->checkTodolistLastItemTimer($todoList) == false){
                $errors .= "Regle des 30min non respectee. ";
            }
            return response()->json($errors, 404);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $exists_item = Item::where('id', $id)->first();
        if(!empty($request->name) && !empty($exists_item)){
            $exists_item->name = $request->name;
        }

        if(!empty($request->content) && !empty($exists_item)){
            $exists_item->content = $request->content;
        }

        if(!empty($exists_item) && $exists_item->isValid() ){
            $exists_item->update();
            return response()->json( 'Item modifie avec success ',200);
        }
        return response()->json('Item non existant ou pas conform', 404);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $exists_item = Item::where('id', $id)->first();

        if(!empty($exists_item)){
            $exists_item->delete();
            return response()->json('Item supprime avec succes', 200); 
        }
        return response()->json('Cette item n\'existe pas', 404);
    }
}
