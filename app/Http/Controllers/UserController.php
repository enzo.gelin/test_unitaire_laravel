<?php

namespace App\Http\Controllers;


use App\Models\User;
use Illuminate\Http\Request;
use App\Services\ToDoListService;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $user = new User();
        $user->lastname = $request->lastname;
        $user->firstname = $request->firstname;
        $user->birthday = $request->birthday;
        $user->email = $request->email;
        $user->email_verified_at = $request->email_verified_at;
        $user->password = $request->password;

        
      

        if($user->isValid() == true ){

            $data = array(
                'lastname'=>$user->lastname,
                'firstname'=>$user->firstname,
                'birthday'=>$user->birthday,
                'email'=>$user->email,
                'email_verified_at'=>$user->email_verified_at,
                'password' => $user->password,
            );

            User::create($data);

            return response()->json("Tout est ok : user bien ajoute", 200);
        }
        return response()->json("user non valid", 404);
    }
}    