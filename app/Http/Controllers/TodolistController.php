<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Todolist;
use App\Models\User;
use Illuminate\Http\Request;
use App\Services\ToDoListService;
use Illuminate\Support\Facades\Auth;

class TodolistController extends Controller
{
    private $customServiceInstance;

    public function __construct(ToDoListService $customServiceInstance)
    {
        $this->customServiceInstance = $customServiceInstance;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // echo $customServiceInstance->verifFirst();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // Check if user have a todolist already

        $verif = $this->customServiceInstance->verifFirst($request->user_id);

        $todolist = new Todolist();
        $todolist->name = $request->name;
        $todolist->user_id = $request->user_id;
      

        if($verif == true && $todolist->isValid()){

            $data = array(
                'name'      =>  $todolist->name,
                'user_id'   =>   $todolist->user_id
            );

            Todolist::create($data);

            return response()->json("Tout est ok : item bien ajoute", 200);
        }
        return response()->json("This user already has a todolist", 404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
