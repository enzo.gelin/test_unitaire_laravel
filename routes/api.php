<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\TodolistController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/', [HomeController::class, 'index']);
Route::post('/create_to_do_list', [TodolistController::class, 'store'])->name('create_to_do_list');
Route::post('/create_user', [UserController::class, 'store'])->name('create_user');
Route::post('/insert_item/{todolist}', [ItemController::class, 'store'])->name('insert_item');
Route::post('/update_item/{id}', [ItemController::class, 'update'])->name('update_item');
Route::post('/delete_item/{id}', [ItemController::class, 'delete'])->name('delete_item');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
