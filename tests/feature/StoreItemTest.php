<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use App\Models\Todolist;
use App\Models\Item;
use App\Models\User;
use Carbon\Carbon;

class StoreItemTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_an_item_storage_with_ALL_satisfied_constraints()
    
    {
        $user = User::factory()->create([
            'id'=>1,
            'lastname'=>"LOREM",
            'firstname'=>"Epsum",
            'birthday'=>Carbon::now()->subYears(20),
            'email'=>"epsum@gmail.com",
        ]);
        $todolist = Todolist::factory()->create([
            'id'=>1,
            'user_id'=>$user->id,
        ]);
        Item::factory()->create([
            'name' => 'impocont_'.rand(),
            'user_id' => $user->id,
            'todolist_id' => $todolist->id,
            'datecreated' => Carbon::now()->subHour(),
        ]);
        $item = [
            "name" => "my content",
            "content" => "ggygyggègyygygy",
            "user_id"=>$user->id,
            "todolist_id"=>$todolist->id,
            'datecreated' => Carbon::now(),
        ];

        $response = $this->post('api/insert_item/'.$item["todolist_id"],$item);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function test_an_item_storage_with_NON_satisfied_30Min_constraints()
    
    {
        $user = User::factory()->create([
            'id'=>1,
            'lastname'=>"LOREM",
            'firstname'=>"Epsum",
            'birthday'=>Carbon::now()->subYears(20),
            'email'=>"epsum@gmail.com",
        ]);
        $todolist = Todolist::factory()->create([
            'id'=>1,
            'user_id'=>$user->id,
        ]);
        Item::factory()->create([
            'name' => 'impocont_'.rand(),
            'user_id' => $user->id,
            'todolist_id' => $todolist->id,
            'datecreated' => Carbon::now(),
        ]);
        $item = [
            "name" => "my content",
            "content" => "ggygyggègyygygy",
            "user_id"=>$user->id,
            "todolist_id"=>$todolist->id,
            'datecreated' => Carbon::now(),
        ];

        $response = $this->post('api/insert_item/'.$item["todolist_id"],$item);
        $this->assertEquals(404, $response->getStatusCode());
    }
    

    public function test_an_item_storage_with_NON_satisfied_content_constraints()
    
    {
        $user = User::factory()->create([
            'id'=>1,
            'lastname'=>"LOREM",
            'firstname'=>"Epsum",
            'birthday'=>Carbon::now()->subYears(20),
            'email'=>"epsum@gmail.com",
        ]);
        $todolist = Todolist::factory()->create([
            'id'=>1,
            'user_id'=>$user->id,
        ]);
        Item::factory()->create([
            'name' => 'impocont_'.rand(),
            'user_id' => $user->id,
            'todolist_id' => $todolist->id,
            'datecreated' => Carbon::now(),
        ]);
        $item = [
            "name" => "my content",
            "content" => "",
            "user_id"=>$user->id,
            "todolist_id"=>$todolist->id,
            'datecreated' => Carbon::now()->addHour(),
        ];

        $response = $this->post('api/insert_item/'.$item["todolist_id"],$item);
        $this->assertEquals(404, $response->getStatusCode());
    }

    public function test_an_item_storage_with_NON_satisfied_user_age_constraints()
    
    {
        $user = User::factory()->create([
            'id'=>1,
            'lastname'=>"LOREM",
            'firstname'=>"Epsum",
            'birthday'=>Carbon::now(),
            'email'=>"epsum@gmail.com",
        ]);
        $todolist = Todolist::factory()->create([
            'id'=>1,
            'user_id'=>$user->id,
        ]);
        Item::factory()->create([
            'name' => 'impocont_'.rand(),
            'user_id' => $user->id,
            'todolist_id' => $todolist->id,
            'datecreated' => Carbon::now(),
        ]);
        $item = [
            "name" => "my content",
            "content" => "ggygyggègyygygy",
            "user_id"=>$user->id,
            "todolist_id"=>$todolist->id,
            'datecreated' => Carbon::now()->addHour(),
        ];

        $response = $this->post('api/insert_item/'.$item["todolist_id"],$item);
        $this->assertEquals(404, $response->getStatusCode());
    }

    public function test_an_item_storage_with_NON_satisfied_user_min_length_constraints()
    
    {
        $user = User::factory()->create([
            'id'=>1,
            'lastname'=>"LO",
            'firstname'=>"Ep",
            'birthday'=>Carbon::now(),
            'email'=>"epsum@gmail.com",
        ]);
        $todolist = Todolist::factory()->create([
            'id'=>1,
            'user_id'=>$user->id,
        ]);
        Item::factory()->create([
            'name' => 'impocont_'.rand(),
            'user_id' => $user->id,
            'todolist_id' => $todolist->id,
            'datecreated' => Carbon::now(),
        ]);
        $item = [
            "name" => "my content",
            "content" => "ggygyggègyygygy",
            "user_id"=>$user->id,
            "todolist_id"=>$todolist->id,
            'datecreated' => Carbon::now()->addHour(),
        ];

        $response = $this->post('api/insert_item/'.$item["todolist_id"],$item);
        $this->assertEquals(404, $response->getStatusCode());
    }

    public function test_an_item_storage_with_NON_satisfied_user_email_constraints()
    
    {
        $user = User::factory()->create([
            'id'=>1,
            'lastname'=>"LO",
            'firstname'=>"Ep",
            'birthday'=>Carbon::now(),
            'email'=>"epsum@",
        ]);
        $todolist = Todolist::factory()->create([
            'id'=>1,
            'user_id'=>$user->id,
        ]);
        Item::factory()->create([
            'name' => 'impocont_'.rand(),
            'user_id' => $user->id,
            'todolist_id' => $todolist->id,
            'datecreated' => Carbon::now(),
        ]);
        $item = [
            "name" => "my content",
            "content" => "ggygyggègyygygy",
            "user_id"=>$user->id,
            "todolist_id"=>$todolist->id,
            'datecreated' => Carbon::now()->addHour(),
        ];

        $response = $this->post('api/insert_item/'.$item["todolist_id"],$item);
        $this->assertEquals(404, $response->getStatusCode());
    }

    public function test_an_item_storage_with_NON_satisfied_todolist_limit_10_constraints()
    
    {
        $user = User::factory()->create([
            'id'=>1,
            'lastname'=>"LO",
            'firstname'=>"Ep",
            'birthday'=>Carbon::now(),
            'email'=>"epsum@",
        ]);
        $todolist = Todolist::factory()->create([
            'id'=>1,
            'user_id'=>$user->id,
        ]);
        Item::factory()->count(10)->create([
            'user_id' => $user->id,
            'todolist_id' => $todolist->id,
            'datecreated' => Carbon::now(),
        ]);
        $item = [
            "name" => "my content",
            "content" => "ggygyggègyygygy",
            "user_id"=>$user->id,
            "todolist_id"=>$todolist->id,
            'datecreated' => Carbon::now()->addHour(),
        ];

        $response = $this->post('api/insert_item/'.$item["todolist_id"],$item);
        $this->assertEquals(404, $response->getStatusCode());
    }

    
}