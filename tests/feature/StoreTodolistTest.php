<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use App\Models\Todolist;
use App\Models\User;
use Carbon\Carbon;

class StorTodolistTest extends TestCase
{
    use RefreshDatabase;

    public function test_a_todolist_storage_with_ALL_satisfied_constraints()
    
    {
        $user = User::factory()->create([
            'id'=>1,
            'lastname'=>"LOREM",
            'firstname'=>"Epsum",
            'birthday'=>Carbon::now()->subYears(20),
            'email'=>"epsum@gmail.com",
        ]);
       
        $todolist = [
            "name" => "my content",
            "user_id"=>$user->id,
        ];

        $response = $this->post('api/create_to_do_list/',$todolist);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function test_a_todolist_storage_with_user_already_has_one_todolist()
    
    {
        $user = User::factory()->create([
            'id'=>1,
            'lastname'=>"LOREM",
            'firstname'=>"Epsum",
            'birthday'=>Carbon::now()->subYears(20),
            'email'=>"epsum@gmail.com",
        ]);

        Todolist::factory()->create([
            "name" => "first todolist",
            "user_id"=>$user->id,
        ]);
       
        $todolist = [
            "name" => "my content",
            "user_id"=>$user->id,
        ];

        $response = $this->post('api/create_to_do_list/',$todolist);
        $this->assertEquals(404, $response->getStatusCode());
    }

    public function test_a_todolist_storage_with_NON_satistied_name_min_length_constraints()
    
    {
        $user = User::factory()->create([
            'id'=>1,
            'lastname'=>"LOREM",
            'firstname'=>"Epsum",
            'birthday'=>Carbon::now()->subYears(20),
            'email'=>"epsum@gmail.com",
        ]);
       
        $todolist = [
            "name" => "",
            "user_id"=>$user->id,
        ];

        $response = $this->post('api/create_to_do_list/',$todolist);
        $this->assertEquals(404, $response->getStatusCode());
    }

}