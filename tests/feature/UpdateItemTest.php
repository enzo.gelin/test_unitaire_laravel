<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use App\Models\Todolist;
use App\Models\Item;
use App\Models\User;
use Carbon\Carbon;


class UpdateItemTest extends TestCase
{
    use RefreshDatabase;

    public function test_an_item_update_with_ALL_satisfied_constraints()
    
    {
        $user = User::factory()->create([
            'id'=>1,
            'lastname'=>"LOREM",
            'firstname'=>"Epsum",
            'birthday'=>Carbon::now()->subYears(20),
            'email'=>"epsum@gmail.com",
        ]);
        $todolist = Todolist::factory()->create([
            'id'=>1,
            'user_id'=>$user->id,
        ]);
        Item::factory()->create([
            'id' => 1,
            'name' => 'impocont_'.rand(),
            'user_id' => $user->id,
            'todolist_id' => $todolist->id,
            'datecreated' => Carbon::now(),
        ]);

        $item = [
            'id'=> 1,
            "name" => "i want to change it",
            "content" => "user may want to change this as well"
        ];

        $response = $this->post('api/update_item/'.$item['id'],$item);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function test_a_NON_existant_item_update_with_ALL_satisfied_constraints()
    
    {
        $user = User::factory()->create([
            'id'=>1,
            'lastname'=>"LOREM",
            'firstname'=>"Epsum",
            'birthday'=>Carbon::now()->subYears(20),
            'email'=>"epsum@gmail.com",
        ]);
        $todolist = Todolist::factory()->create([
            'id'=>1,
            'user_id'=>$user->id,
        ]);

        $item = [
            'id'=> 1,
            "name" => "i want to change it",
            "content" => "user may want to change this as well"
        ];

        $response = $this->post('api/update_item/'.$item['id'],$item);
        $this->assertEquals(404, $response->getStatusCode());
    }

}    