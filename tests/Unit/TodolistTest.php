<?php


namespace Tests\Unit;

use App\Models\Item;
use App\Models\Todolist;
use App\Models\User;
use App\Services\ToDoListService;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;
use function PHPUnit\Framework\assertTrue;

class TodolistTest extends TestCase
{

    private $user;
    private $item;
    private $todolist;
    private $nonValidItem;
    private $newValidItem;

    protected function setUp(): void{

        parent::setUp();

        $this->item = new Item([
            'name' => 'Faire ses courses',
            'content' => 'acheter des madeleines',
            'datecreated'=> Carbon::now()->subHour()
        ]);

        $this->nonValidItem = new Item([
            'name' => 'item non valid car contenu vide',
            'content' => '',
            'datecreated' => Carbon::now()
        ]);

        $this->newValidItem = new Item([
            'name' => 'item non valid car contenu vide',
            'content' => 'any content',
            'datecreated' => Carbon::now()
        ]);

        $this->user = new User([
            'lastname'=>'john',
            'firstname'=>'doe',
            'birthday'=>Carbon::now()->subDecades(2)->subMonths(3),
            'email'=>'johndoe@gmail.com',
            'password'=>'password',
        ]);

        $this->todolist = $this->getMockBuilder(Todolist::class)
            ->onlyMethods(['actualItemsCount','getLastItem'])
            ->getMock();

        $this->todolist->user = $this->user;
        $this->todolist->name = "a name here";
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */

    public function testCheckValidItem(){
        $isValid = $this->item->isValid();
        $this->assertTrue($isValid);   
    }

    public function testCheckNonValidItem(){
        $isValid = $this->nonValidItem->isValid();
        $this->assertEquals(false,$isValid);
    }

    //check if the number of todolist items is <= 10
    public function testCheckValidTodoListLength(){

        $this->todolist->expects($this->any())->method('actualItemsCount')->willReturn(1);
        $this->assertEquals(true,$this->todolist->isLengthValid());

    }

    public function testCheckNonValidTodoListLength(){

        $this->todolist->expects($this->any())->method('actualItemsCount')->willReturn(10);
        $this->assertEquals(false,$this->todolist->isLengthValid());

    }

    //Check the 30 minutes condition
    public function testCheckTodolistLastItemTimer()
    {
        $this->todolist->expects($this->any())->method('getLastItem')->willReturn($this->item);

        $this->assertEquals(true,$this->todolist->checkTodolistLastItemTimer());
    }

    public function testCheckTodolistNonValidLastItemTimer()
    {
        $this->todolist->expects($this->any())->method('getLastItem')->willReturn($this->nonValidItem);

        $this->assertEquals(false,$this->todolist->checkTodolistLastItemTimer());
    }

    //check all the condition of Item adding
    public function testCanAddItem(){

        $this->todolist->expects($this->any())->method('getLastItem')->willReturn($this->item);
        $this->todolist->expects($this->any())->method('actualItemsCount')->willReturn(1);

        $this->assertEquals(true,$this->todolist->canAddItem($this->newValidItem));

    }

    //check all the condition of Item adding
    public function testCanTAddItemTooSoonNewOne(){
        $this->item->datecreated = Carbon::now();

        $this->todolist->expects($this->any())->method('getLastItem')->willReturn($this->item);
        $this->todolist->expects($this->any())->method('actualItemsCount')->willReturn(1);

        $this->assertEquals(false,$this->todolist->canAddItem($this->newValidItem));

    }

    //check all the condition of Item adding
    public function testCanTAddItemFullList(){

        $this->todolist->expects($this->any())->method('getLastItem')->willReturn($this->item);
        $this->todolist->expects($this->any())->method('actualItemsCount')->willReturn(10);

        $this->assertEquals(false,$this->todolist->canAddItem($this->newValidItem));
    }

    //check all the condition of Item adding
    public function testCanTAddItemInvalidOne(){

        $this->todolist->expects($this->any())->method('getLastItem')->willReturn($this->item);
        $this->todolist->expects($this->any())->method('actualItemsCount')->willReturn(10);

        $this->assertEquals(false,$this->todolist->canAddItem($this->nonValidItem));
    }
}
