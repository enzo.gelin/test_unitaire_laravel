<?php

namespace Tests\Unit;

use App\Models\User;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testCheckMail()
    {
        $user = new User;
        $user->email = 'Titre@mail.fr';
        $this->assertTrue($user->checkMail());
    }

    public function testCheckNonValidMail()
    {
        $user = new User;
        $user->email = 'Titre@mail';
        $this->assertEquals(false,$user->checkMail());
    }

    // Verifie l'intégrité du nom et prenom
    public function testCheckName()
    {
        $user = new User;
        $user->firstname = 'Jena';
        $user->lastname = 'Paul';
        $this->assertTrue($user->checkName());
    }
    public function testCheckNonValidLNameFName()
    {
        $user = new User;
        $user->firstname = 'J';
        $user->lastname = 'Pa';
        $this->assertEquals(false,$user->checkName());
    }

    // Verifie l'intégrité de la date et +18 ans
    public function testCheckBirthday()
    {
        $user = new User;
        $user->birthday = Carbon::now()->subYears(20);
        $this->assertTrue($user->checkBirthday());
    }

    public function testCheckNonValidBirthday()
    {
        $user = new User;
        $user->birthday = Carbon::now()->subYears(5);
        $this->assertEquals(false,$user->checkBirthday());
    }

    public function testCheckPassword()
    {
        $user = new User;
        $user->password = 'redshadowlegend';
        $this->assertTrue($user->checkPassword());
    }

    public function testCheckNonValidPassword()
    {
        $user = new User;
        $user->password = 'red';
        $this->assertEquals(false,$user->checkPassword());
    }

    public function testValidUser()
    {
        $user = new User;
        $user->email = 'Titre@mail.fr';
        $user->firstname = 'Jena';
        $user->lastname = 'Paul';
        $user->password = 'redshadowlegend';
        $user->birthday = Carbon::now()->subYears(20);

        $testname = $user->checkName();
        $testemail = $user->checkMail();
        $testpwd = $user->checkPassword();

        $this->assertTrue($user->checkName() && $user->checkMail() && $user->checkPassword() && $user->checkBirthday());
    }

    public function testNonValidUser()
    {
        $user = new User;
        $user->email = 'Titre@mail';
        $user->firstname = 'Jena';
        $user->lastname = 'P';
        $user->password = 'redshadowlegend';
        $user->birthday = Carbon::now()->subYears();

        $testname = $user->checkName();
        $testemail = $user->checkMail();
        $testpwd = $user->checkPassword();

        $this->assertEquals(false,$user->checkName() && $user->checkMail() && $user->checkPassword() && $user->checkBirthday());
    }
}
