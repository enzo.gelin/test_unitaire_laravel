<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Str;
use Tests\TestCase;
use App\Models\User;
use Carbon\Carbon;

class StoreUserTest extends TestCase
{
    use RefreshDatabase;

    public function test_a_user_storage_with_ALL_satisfied_constraints()
    
    {
        $user = [
            'lastname'=>"LOREM",
            'firstname'=>"Epsum",
            'birthday'=> Carbon::now()->subYears(20),
            'email'=>"epsum@gmail.com",
            'email_verified_at'=> Carbon::now()->subHour(),
            'password' => 'password',
        ];

        $response = $this->post('api/create_user/',$user);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function test_a_user_storage_with_NON_satisfied_name_constraints()
    
    {
        $user = [
            'lastname'=>"LO",
            'firstname'=>"E",
            'birthday'=> Carbon::now()->subYears(20),
            'email'=>"epsum@gmail.com",
            'email_verified_at'=> Carbon::now()->subHour(),
            'password' => 'password',
        ];

        $response = $this->post('api/create_user/',$user);
        $this->assertEquals(404, $response->getStatusCode());
    }

    public function test_a_user_storage_with_NON_satisfied_email_constraints()
    
    {
        $user = [
            'lastname'=>"LOREM",
            'firstname'=>"EPSUM",
            'birthday'=> Carbon::now()->subYears(20),
            'email'=>"epsum@com",
            'email_verified_at'=> Carbon::now()->subHour(),
            'password' => 'password',
        ];

        $response = $this->post('api/create_user/',$user);
        $this->assertEquals(404, $response->getStatusCode());
    }

    public function test_a_user_storage_with_NON_satisfied_password_constraints()
    
    {
        $user = [
            'lastname'=>"LOREM",
            'firstname'=>"EPSUM",
            'birthday'=> Carbon::now()->subYears(20),
            'email'=>"epsum@myges.com",
            'email_verified_at'=> Carbon::now()->subHour(),
            'password' => 'pass',
        ];

        $response = $this->post('api/create_user/',$user);
        $this->assertEquals(404, $response->getStatusCode());
    }

    public function test_a_user_storage_with_NON_satisfied_age_constraints()
    
    {
        $user = [
            'lastname'=>"LOREM",
            'firstname'=>"EPSUM",
            'birthday'=> Carbon::now()->subYears(1),
            'email'=>"epsum@myges.com",
            'email_verified_at'=> Carbon::now()->subHour(),
            'password' => 'pass',
        ];

        $response = $this->post('api/create_user/',$user);
        $this->assertEquals(404, $response->getStatusCode());
    }

}    