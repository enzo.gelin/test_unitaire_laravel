<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use App\Models\Todolist;
use App\Models\Item;
use App\Models\User;
use Carbon\Carbon;



class DeleteteItemTest extends TestCase
{
    use RefreshDatabase;

    public function test_an_item_delete_with_ALL_satisfied_constraints()
    {
        $user = User::factory()->create([
            'id'=>1,
            'lastname'=>"LOREM",
            'firstname'=>"Epsum",
            'birthday'=>Carbon::now()->subYears(20),
            'email'=>"epsum@gmail.com",
        ]);
        $todolist = Todolist::factory()->create([
            'id'=>1,
            'user_id'=>$user->id,
        ]);
        $item = Item::factory()->create([
            'id' => 1,
            'name' => 'impocont_'.rand(),
            'user_id' => $user->id,
            'todolist_id' => $todolist->id,
            'datecreated' => Carbon::now(),
        ]);
        $response = $this->post('api/delete_item/'.$item['id']);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function test_a_NON_existing_item_delete_with_ALL_satisfied_constraints()
    {
        $user = User::factory()->create([
            'id'=>1,
            'lastname'=>"LOREM",
            'firstname'=>"Epsum",
            'birthday'=>Carbon::now()->subYears(20),
            'email'=>"epsum@gmail.com",
        ]);
        $todolist = Todolist::factory()->create([
            'id'=>1,
            'user_id'=>$user->id,
        ]);
        
        //this item without factory is not existing in database
        $item = [
            'id' => 1
        ];

        $response = $this->post('api/delete_item/'.$item['id']);
        $this->assertEquals(404, $response->getStatusCode());
    }
}