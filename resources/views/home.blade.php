@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"> Ma rodolist : {{ $todolist[0]->name }}</div>

                <div class="card-body">
                @foreach($items as $item)
                    
                        <div class="alert alert-success" role="alert">
                            <li> {{ $item->content }}</li>
                        </div>
                        
                    
                @endforeach
                    
                        

                </div>
                <div class="card-footer">
                 <li><a href="/create"> Creer une todolist </a></li>|<li> <a href="#">Supprimer ma todolist</a> </li>| <li><a href="/insert_item/{{ $todolist[0]->id }}">Ajouter des items</a></li></div>

            </div>
        </div>
    </div>
</div>
@endsection
